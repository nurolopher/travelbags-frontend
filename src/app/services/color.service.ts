import { Injectable } from '@angular/core';
import {AppContextService} from "./app-context.service";
import {Http} from '@angular/http';
import {Color} from "../entity/Color";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ColorService extends AppContextService {

  constructor(private http:Http) {
    super();
  }

  getColors():Promise<Color[]> {
    return this.http
      .get(this.getBaseURL() + '/colors')
      .toPromise()
      .then(colors => colors.json())
      .catch(this.handleError);
  }
}
