/**
 * Created by nursultan on 6/28/16.
 */

import {Color} from "./Color";
import {Mark} from "./Mark";
import {Material} from "./Material";
import {Category} from "./Category";

export class Filter {
  constructor(public  colors?:Color[],
              public marks?:Mark[],
              public materials?:Material[],
              public categories?:Category[]) {

  }
}
