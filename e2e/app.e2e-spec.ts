import { TravelbagPage } from './app.po';

describe('travelbag App', function() {
  let page: TravelbagPage;

  beforeEach(() => {
    page = new TravelbagPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
