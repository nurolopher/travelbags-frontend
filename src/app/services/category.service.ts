import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AppContextService} from './app-context.service';
import {Category} from "../entity/Category";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CategoryService extends AppContextService{

  constructor(private http:Http) {
    super();
  }

  getCategories():Promise<Category[]> {
    return this.http
      .get(this.getBaseURL() + '/categories')
      .toPromise()
      .then((category)=>category.json())
      .catch(this.handleError);
  }
}
