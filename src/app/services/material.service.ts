import { Injectable } from '@angular/core';
import {AppContextService} from "./app-context.service";
import {Http} from '@angular/http';
import {Material} from "../entity/Material";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class MaterialService extends AppContextService {

  constructor(private http:Http) {
    super();
  }

  getMaterials():Promise<Material[]> {
    return this.http.get(this.getBaseURL() + '/materials')
      .toPromise()
      .then(materials => materials.json())
      .catch(this.handleError);
  }
}
