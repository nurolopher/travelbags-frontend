import { Component, OnInit } from '@angular/core';
import {Filter} from "../../entity/Filter";
import {MarkService} from "../../services/mark.service";
import {ColorService} from "../../services/color.service";
import {MaterialService} from "../../services/material.service";
import {Mark} from "../../entity/Mark";
import {Color} from "../../entity/Color";
import {Material} from "../../entity/Material";
import {BagContainerService} from "../../services/bag-container.service";
import {BagService} from "../../services/bag.service";
import {Router} from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-filter-form',
  templateUrl: 'filter-form.component.html',
  styleUrls: ['filter-form.component.css']
})
export class FilterFormComponent implements OnInit {

  filter:Filter;
  marks:Mark[];
  colors:Color[];
  materials:Material[];

  constructor(private markService:MarkService,
              private colorService:ColorService,
              private materialService:MaterialService,
              private bagService:BagService,
              private bagContainer:BagContainerService,
              private router:Router) {
    this.filter = new Filter();
  }

  ngOnInit() {
    this.markService.getMarks().then(marks => this.marks = marks);
    this.colorService.getColors().then(colors => this.colors = colors);
    this.materialService.getMaterials().then(materials => this.materials = materials);
  }

  submit(form) {
    let queryParams = {};
    jQuery(form).serializeArray().forEach(function (param) {
      queryParams[param.name] = param.value;
    });
    this.router.routerState.queryParams.subscribe(params => {
        this.router.navigate(['bags'], {queryParams: queryParams});
        this.bagService.getBags("?" + jQuery.param(queryParams)).then(bags => this.bagContainer.bags = bags);
      })
      .unsubscribe();
  }
}
