import { Injectable } from '@angular/core';
import {AbstractEntityService} from "./abstract/AbstractEntityService";

@Injectable()
export class AppContextService extends AbstractEntityService {
  private baseURL = 'http://localhost:8000/api';
  constructor() {
    super();
  }

  public getBaseURL() {
    return this.baseURL;
  }

}
