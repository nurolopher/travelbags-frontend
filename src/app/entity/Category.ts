/**
 * Created by nursultan on 6/27/16.
 */
export class Category {
  public id:number;
  public title:string;
  public slug:string;
}
