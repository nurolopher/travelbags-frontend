/**
 * Created by nursultan on 6/27/16.
 */
export class Bag {
  public id:number;
  public title:string;
  public description:string;
  public slug:string
  public price:string;
}
