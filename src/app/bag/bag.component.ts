import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute } from '@angular/router';
import {BagService} from "../services/bag.service";
import {Bag} from "../entity/Bag";
@Component({
  moduleId: module.id,
  selector: 'app-bag',
  templateUrl: 'bag.component.html',
  styleUrls: ['bag.component.css']
})
export class BagComponent implements OnInit {

  bag:Bag;

  constructor(private route:ActivatedRoute,
              private bagService:BagService) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let slug = params['slug'];
      this.bagService.getBag(slug)
        .then(bag => {
          this.bag = bag;
        });
    });
  }

}
