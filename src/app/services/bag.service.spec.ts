/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import { BagService } from './bag.service';

describe('Bag Service', () => {
  beforeEachProviders(() => [BagService]);

  it('should ...',
      inject([BagService], (service: BagService) => {
    expect(service).toBeTruthy();
  }));
});
