import {AppContextService} from "../app-context.service";
/**
 * Created by nursultan on 6/27/16.
 */
export class AbstractEntityService{
  protected handleError(error:any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
