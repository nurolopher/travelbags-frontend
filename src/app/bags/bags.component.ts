import { Component, OnInit, OnDestroy } from '@angular/core';
import {Router,ActivatedRoute } from '@angular/router';
import {BagService} from "../services/bag.service";
import {Bag} from "../entity/Bag";
import {BagContainerService} from "../services/bag-container.service";


import {FilterFormComponent} from "../form/filter-form/filter-form.component";
import {MarkService} from "../services/mark.service";
import {ColorService} from "../services/color.service";
import {MaterialService} from "../services/material.service";
import {CategoryService} from "../services/category.service";

@Component({
  moduleId: module.id,
  selector: 'app-bags',
  templateUrl: 'bags.component.html',
  styleUrls: ['bags.component.css'],
  directives: [
    FilterFormComponent
  ],
  providers: [MarkService, ColorService, MaterialService, CategoryService]
})
export class BagsComponent implements OnInit, OnDestroy {

  sub:any;
  bags:Bag[];

  constructor(private route:ActivatedRoute,
              private router:Router,
              private bagService:BagService,
              public bagContainer:BagContainerService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let queryParams = location.search ? location.search : '?';
      queryParams += 'filter[category]=' + params['slug'];
      this.bagService.getBags(queryParams).then(bags => this.bagContainer.bags = bags);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  select(bag:Bag) {
    this.router.navigate(['/bag', bag.slug]);
  }
}
