import { Injectable } from '@angular/core';
import {Bag} from "../entity/Bag";
import {Filter} from "../entity/Filter";

@Injectable()
export class BagContainerService {

  public bags:Bag[];
  public filter:Filter;

  constructor() {
  }

}
