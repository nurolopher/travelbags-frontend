import { Injectable } from '@angular/core';
import {AppContextService} from "./app-context.service";
import {Http} from '@angular/http';
import {Mark} from "../entity/Mark";

import 'rxjs/add/operator/toPromise';
@Injectable()
export class MarkService extends AppContextService {

  constructor(private http:Http) {
    super();
  }

  getMarks():Promise<Mark[]> {
    return this.http
      .get(this.getBaseURL() + '/marks')
      .toPromise()
      .then(marks => marks.json())
      .catch(this.handleError);
  }
}
