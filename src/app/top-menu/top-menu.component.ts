import { Component, OnInit, Input } from '@angular/core';
import {CategoryService} from "../services/category.service";
import {Category} from "../entity/Category";
import {Router, ROUTER_DIRECTIVES} from "@angular/router";
import {BagContainerService} from "../services/bag-container.service";
import {BagService} from "../services/bag.service";

@Component({
  moduleId: module.id,
  selector: 'app-top-menu',
  templateUrl: 'top-menu.component.html',
  styleUrls: ['top-menu.component.css'],
  directives: [ROUTER_DIRECTIVES]
})
export class TopMenuComponent implements OnInit {

  categories:Category[];
  error:any;

  constructor(private categoryService:CategoryService) {
  }


  ngOnInit() {
    this.getCategories();
  }

  private getCategories():void {
    this.categoryService
      .getCategories()
      .then((categories) => this.categories = categories)
      .catch((error)=>this.error = error);
  }
}
