/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import { BagContainerService } from './bag-container.service';

describe('BagContainer Service', () => {
  beforeEachProviders(() => [BagContainerService]);

  it('should ...',
      inject([BagContainerService], (service: BagContainerService) => {
    expect(service).toBeTruthy();
  }));
});
