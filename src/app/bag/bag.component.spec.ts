/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';

import { BagComponent } from './bag.component';

describe('Component: Bag', () => {
  it('should create an instance', () => {
    let component = new BagComponent();
    expect(component).toBeTruthy();
  });
});
