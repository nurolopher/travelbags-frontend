import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AppContextService} from "./app-context.service";
import {AbstractEntityService} from "./abstract/AbstractEntityService";
import {Bag} from "../entity/Bag";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class BagService extends AppContextService {

  constructor(private http:Http) {
    super();
  }

  getBags(params?:string):Promise<Bag[]> {
    return this.http
      .get(this.getBaseURL() + '/bags' + params)
      .toPromise()
      .then(bags => bags.json())
      .catch(this.handleError);

  }

  getBag(slug:string):Promise<Bag> {
    return this.http
      .get(this.getBaseURL() + '/bags/' + slug)
      .toPromise()
      .then(bag => bag.json())
      .catch(this.handleError);
  }
}
