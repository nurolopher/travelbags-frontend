/**
 * Created by nursultan on 6/27/16.
 */
import {provideRouter, RouterConfig } from '@angular/router';
import {BagsComponent} from "./bags/bags.component";
import {BagComponent} from "./bag/bag.component";

export const routes:RouterConfig = [
  {path: 'bags/:slug', component: BagsComponent},
  {path: 'bag/:slug', component: BagComponent}
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
