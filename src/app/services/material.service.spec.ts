/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import { MaterialService } from './material.service';

describe('Material Service', () => {
  beforeEachProviders(() => [MaterialService]);

  it('should ...',
      inject([MaterialService], (service: MaterialService) => {
    expect(service).toBeTruthy();
  }));
});
