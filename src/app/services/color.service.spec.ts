/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import { ColorService } from './color.service';

describe('Color Service', () => {
  beforeEachProviders(() => [ColorService]);

  it('should ...',
      inject([ColorService], (service: ColorService) => {
    expect(service).toBeTruthy();
  }));
});
