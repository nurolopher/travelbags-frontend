/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import { MarkService } from './mark.service';

describe('Mark Service', () => {
  beforeEachProviders(() => [MarkService]);

  it('should ...',
      inject([MarkService], (service: MarkService) => {
    expect(service).toBeTruthy();
  }));
});
