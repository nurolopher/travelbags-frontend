/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import { AppContextService } from './app-context.service';

describe('AppContext Service', () => {
  beforeEachProviders(() => [AppContextService]);

  it('should ...',
      inject([AppContextService], (service: AppContextService) => {
    expect(service).toBeTruthy();
  }));
});
