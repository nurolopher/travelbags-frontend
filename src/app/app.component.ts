import { Component } from '@angular/core';
import {TopMenuComponent} from "./top-menu/top-menu.component";

import {Category} from "./entity/Category";

import {CategoryService} from "./services/category.service";
import {AppContextService} from "./services/app-context.service";
import {BagService} from "./services/bag.service";
import {BagContainerService} from "./services/bag-container.service";

import {ROUTER_DIRECTIVES} from '@angular/router';
@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [
    TopMenuComponent,
    ROUTER_DIRECTIVES],
  providers: [
    CategoryService,
    BagService,
    AppContextService,
    BagContainerService
  ]
})
export class AppComponent {
  constructor() {
  }
}
